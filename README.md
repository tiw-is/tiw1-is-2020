Université Claude Bernard Lyon 1 – M2 TIW – Intergiciels et Services

# TIW1 - Intergiciels et services

Ce dépôt regroupe les ressources pour l'UE [intergiciels et services](http://offre-de-formations.univ-lyon1.fr/ue-16806-345%2Fintergiciels-et-services.html) du master [TIW](http://master-info.univ-lyon1.fr/TIW/).

## Supports de cours

- [Slides introduction + TP1](http://emmanuel.coquery.pages.univ-lyon1.fr/slides/tiw1-01-introduction/#/)
- [Slides rappels Spring](http://emmanuel.coquery.pages.univ-lyon1.fr/slides/tiw1-01b-revision-spring/#/)
- [Slides Spring Data & Security](http://emmanuel.coquery.pages.univ-lyon1.fr/slides/tiw1-08-spring-data-security/#/)
- Conteneurs et Inversion de Contrôle : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_conteneurs.pdf), [vidéo](https://tube.ac-lyon.fr/videos/watch/95027c0b-c407-4129-bc37-d73fd32c74a6)
- Contexte et Annuaire : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_contexte.pdf), [vidéo](https://tube.ac-lyon.fr/videos/watch/95027c0b-c407-4129-bc37-d73fd32c74a6)
- Métaprogrammation : annotations et programmation orientée-aspects : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM-metaprogrammation.pdf), [vidéo](https://tube.ac-lyon.fr/videos/watch/092be347-121e-414b-8325-75fdea2f8772)
- [Objets distribués](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_objets_distribues.pdf), [SOAP](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/tiw1-is/web-services-03a-messages.pdf), [WSDL](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/tiw1-is/webservices-03b-wsdl.pdf)
- Microservices : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_microservices.pdf), [vidéo](https://tube.ac-lyon.fr/videos/watch/4ee9687f-94d6-42cf-85a1-56ae53da42a9)
- [Bus de messages et EIP](http://emmanuel.coquery.pages.univ-lyon1.fr/slides/tiw1-10-messages-eip)
- Frameworks de composants dynamiques (OSGi) : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_OSGi.pdf), [vidéo](https://tube.ac-lyon.fr/videos/watch/75fcf7aa-86c2-4162-a5d0-8ec65de3731e)
- [Kubernetes](http://emmanuel.coquery.pages.univ-lyon1.fr/tiw1-is/OrchestrationConteneursK8S.pdf)
- [Cache](https://perso.liris.cnrs.fr/emmanuel.coquery/enseignement/tiw1-is/CacheApplicatif.pdf)
- Urbanisme et urbanisation des SI : [slides](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/CM/CM_IS_urbanisation.pdf), [vidéo]()

## Slides des intervenants

Ils sont disponibles [ici](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/Slides_intervenants/) (volontairement pas accessible depuis ma page publique)
## TPs

- [TP1 : Révision et compléments Java et déploiements](tp1/tp1.md)
- [TP2 : Conteneurs d'objets et inversion de contrôle](tp2/tp2.md)
- [TP3 : Mise en place d'un framework "moderne"](tp3/tp3.md)
- [TP4 : Services & Communication par messages](tp4/tp4.md)
- [TP5 : OSGi](tp5/tp5.md)
- [TP6 : Orchestration de conteneurs avec Kubernetes](tp6/tp6.md)
- [TP7: Benchmarking et passage à l'échelle](tp7/tp7.md)
