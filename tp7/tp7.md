# TP7: Benchmarking et passage à l'échelle

Ce TP n'est pas à rendre.

On se place dans le contexte des [TP4](../tp4/tp4.md) et [TP6](../tp6/tp6.md).

## Mesure de l'efficacité

### Mise au point sur la machine de développement

Dans un premier temps, on se place sur la machine de développement en lançant les banques, la file rabbitmq, les bases de données via docker-compose.

#### Extraction des logs

On souhaite mesurer les performances des transferts bancaires.

Pour cela, on va commencer par extraire les informations des logs des banques.

> Si vous utilisez votre propre implémentation des banques pour ce TP, veillez à ajouter une ligne de log à la réception d'une demande de virement, ainsi qu'une ligne de log indiquant que le virment est terminé, par exemple lors de la notification du fournisseur.

Si vous utilisez les images `tp6-fourni` (mises à jour récement), vous pourrez voir des lignes de log similaires aux suivantes lors de la réception d'une demande et à la fin d'une demande:

pour la banque à l'origine du transfert:

```
2021-02-02 17:44:52.137  INFO 9 --- [nio-8080-exec-4] f.u.m.tiw1.banque.compte.CompteEndpoint  : Reception d'une demande de virement, compte: 1, reference: reference-22
```

pour la banque recevant le transfert:

```
2021-02-02 17:44:52.313  INFO 8 --- [ntContainer#0-1] f.u.m.t.b.externe.TransfertReceiver      : Received {"compteId":2,"amount":23.0,"reference":"reference-22"}
2021-02-02 17:44:52.347  INFO 8 --- [ntContainer#0-1] f.u.m.tiw1.banque.compte.CompteService   : Notification compte 2: reçu 23.0, ref reference-22
```

Écrire un programme (le _moniteur_) qui lit le log (à priori sur son entrée standard) et écrit les informations pertinentes dans une base postgres.
Comme on souhaite mesurer la performance du transfert, ces informations sont le timestamp du log, la référence du virement et l'action correspondante (par exemple reception de demande ou notification, cette dernière indiquant que le transfert est terminé).

> Remarque 1: on veut pouvoir surveiller les banques indépendament, chacune étant surveillée par sa propre instance de _moniteur_. Par contre on souhaite centraliser les extractions de log dans la même base postgres.

> Remarque 2: si vous le souhaitez, vous pouvez remplacer postgres par MongoDB ou ElasticSearch qui sont probalement plus adaptés au stockage de logs. PostgreSQL est utilisé ici par simplicité, étant donné que c'est la base qui a déjà été utilisée en TIW1 auparavant.

#### Premier benchmark

Après avoir mis en place les _moniteurs_ sur les deux banques, lancez une série de transferts interbancaires afin de collecter des mesures de temps d'exécution de transfert.

Remarques:

- Il faut au moins un compte dans chaque banque et le compte source doit être suffisement alimenté pour permettre d'effectuer tous les transferts de la série
- Vous pouvez utiliser [JMeter](http://jmeter.apache.org/) ou [Gatling](https://gatling.io/) pour lancer les transferts, ce qui vous permettra de régler le nombre de transferts par secondes. Vous pouvez aussi écrire vous-même un petit bout de code en Java ou en Python pour ça.
- Pensez à distinguer chaque transfert par une référence différente afin de pouvoir appairer les demandes (début du transfert) et les notifications (fin du transfert)

Une fois les mesures terminées, effectuer une ou plusieurs requêtes SQL (ou mieux, faites une vue) afin de produire des statistiques de base pour cette mesure (min, max, moyenne, écart-type).

### Dans Kubernetes

#### Récupération des logs via un "sidecar"

Déployez une base de données postgres dans kubernetes pour héberger les logs (ou réutilisez la base de `fournisseur`).

Dans l'image `harbor.tiw1.os.univ-lyon1.fr/tp6-fourni/banque:latest` le log de la banque est dupliqué dans le fichier `/logs/banque.log`.

> Si vous utilisez votre propre image, pensez aussi à dupliquer le log dans un fichier. Vous pouvez utiliser la commande [`tee`](https://www.man7.org/linux/man-pages/man1/tee.1.html) avec un `|` pour cela.

Construisez une image docker avec votre _moniteur_.
Pensez à la rendre paramétrable (base de donnée, nom du fichier de log à lire).

> Si votre moniteur ne fait que lire sur l'entrée standard, vous pouvez utiliser `tail -f fichier | moniteur` pour lire en continu dans le fichier.

Modifiez ensuite le déploiement de vos banques de façon à:

- Lancer un deuxième conteneur avec l'image du _moniteur_
- Partager le répertoire de logs (par exemple `/logs` pour l'image fournie) entre les deux conteneurs via un montage de volume commun. Attention, n'utilisez pas un volume persistant pour cela, mais plutôt un volume `emptyDir` ([doc](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir))

L'objectif est d'implémenter l'architecture décrite ici: https://kubernetes.io/docs/concepts/cluster-administration/logging/#sidecar-container-with-a-logging-agent. Le logging agent est _moniteur_, lapp container est la banque.

#### Job pour lancer le benchmark

Au besoin, créez une image docker contenant le matériel pour lancer les requêtes (JMeter, Gatlin, la bonne version de Python ou de Java, évntuellement votre code, etc).

Lancez ensuite au sein du cluster un [Job](https://kubernetes.io/docs/concepts/workloads/controllers/job/) qui va exécuter cette image dans le cluster Kubernetes pour faire les requêtes sur les banques déployées dans le cluster.

Récoltez les mesures comme précédement (via des requêtes/vues dans la base postgres qui héberge les informations issues des logs).

## Campagne de mesures (pour aller plus loin)

### Configuration du benchmark

Modifiez votre _moniteur_, votre application de benchmark et votre base de données pour:

- Ajouter des paramètres décrivant la configuration, en particulier le rythme des requêtes. Prévoyez des à présent que ces paramètres vont évoluer (par exemple on fera varier plus tard le nombre d'instances des banques). On s'attend ici à une modification de la configuration de l'application/script, du schéma de la base et de l'enregistrement du résultat en base. Remarque: à priori, ces paramètres devront passer dans la référence.
- N'effectuer les requêtes que si elles correspondent à des mesures qui ne sont pas déjà présentes en base. Cela servira à ne pas refaire des mesures déjà prises en cas de redémarrage du benchmark.

### Plan de benchmark

Réfléchir à une série de configurations (pour le moment on ne fait varier que le nombre de requêtes par secondes) permettant de tester la limite des performances du virement. On cherche ici à savoir à partir de quel moment les transferts commencent à ralentir à cause de la charge globale du système.

Modifiez ensuite le déploiement des banques pour augmenter le nombre de réplicats.
Relancez le benchmark et voyez si l'augmentation du nombre de réplicat a permi d'améliorer les performances. Vous pouvez essayer de faire varier les réplicats différement pour les deux banques (par exemple 2 réplicats pour la banque1 et un seul pour la banque2).

Essayez de déterminer une éventuelle stratégie de _scaling_ permettant d'améliorer les performances des transferts.
