# TP6: Orchestration de conteneurs avec Kubernetes

## Introduction

### Rendu

Date limite: **dimanche 17/01/2021 23h59**

Remplir la case `Depot_TP6` sur tomuss avec l'URL de votre projet forge et tagguer la branche de rendu pour ce tp avec le tag `TP6`.

Le rendu doit comporter un répertoire `kubernetes` contenant:

- les différents fichiers yaml utilisés pour les déploiements mentionnés ci-dessous
- des screenshots de votre interface rancher montrant les déploiements, les PVC, les services
- un fichier README.md contenant en particulier une description du rôle de chaque fichier yaml utilisé.
  On prendra soin de détailler le rôle de chaque sous partie (i.e. chaque section séparée par `--- `).
  On évitera de détailler chaque élément de la configuration, mais on pourra expliquer les points notables.

La notation sera basée sur le bon fonctionnement des déploiements mais aussi sur la qualité des explications.
Exemple d'explication concernant le déploiement nginx du tutoriel ([fichier yaml](https://raw.githubusercontent.com/kubernetes/website/master/content/en/examples/application/deployment.yaml)):

> Fichier `application/deployment.yaml`: Ce fichier comporte une seule section qui consiste en un [_Deployment_](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/) qui va piloter un [_ReplicatSet_](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/) qui va lui-même instancier des pods avec un conteneur [nginx](https://hub.docker.com/_/nginx). La configuration prévoit le déploiement de 2 pods et l'exposition du port 80.

### Objectifs

Ce TP vise à expérimenter l'utilisation d'un orchestrateur de conteneurs, ici [Kubernetes](https://kubernetes.io/):

- déploiement de conteneurs et des ressources associées
- élasticité
- reconfiguration et redéploiement

### Infrastructure

Un cluster Kubernetes (`etudiants`) est mis à disposition.
Chaque étudiant recevra un mot de passe via tomuss (le login est le même que le compte étudiant usuel).
Une interface Web de gestion (instance [Rancher](https://rancher.com/)) est disponible à l'adresse https://rancher.tiw1.os.univ-lyon1.fr (le certificat est auto-signé).
Les identifiants de connexion sont votre login étudiant (pxxxxxxx) et le mot de passe spécifié dans tomuss (`rancher_mdp`).

Bien que cette interface permette d'effectuer un certain nombre d'actions, il est demandé d'utiliser plutôt l'interface en ligne de commandes de Kubernetes: [`kubectl`](https://kubernetes.io/docs/tasks/tools/install-kubectl/).
Cet utilitaire nécessite une configuration pour accéder au cluster.
Celle-ci peut être récupérée en cliquant sur "Cluster: etudiants" dans le menu déroulant en haut à gauche, puis dans le Dashboard (accessible via l'onglet Cluster à côté du menu déroulant) et enfin le bouton Kubeconfig file.

Vous pouvez sauvegarder ce fichier et indiquer son emplacement à `kubectl` via la variable d'environnement `$KUBECONFIG`.

Chaque groupe/binôme s'est vu associé un projet au sein du cluster.
Dans ce projet, un _namespace_ de la forme `grp-xx` a été créé.
La commande suivante va permettre de changer la configuration par défaut de `kubectl` pour changer utiliser ce namespace par défaut (ne pas oublier de changer `xx`):

```shell
kubectl config set-context $(kubectl config current-context) --namespace=grp-xx
```

> Il faudra bien penser à créer toutes vos ressources dans ce namespace.

Pour tester que kubectl fonctionne, lancer la commande suivante:

```shell
kubectl get pod
```

Cette commande devrait répondre: "No resources found."

## Premiers déploiements

Il est conseillé de bien garder les fichiers utilisés pour cette partie: il pourront servir de point de départ pour le déploiement des applications des TPs précédents.

Dans la suite du TP, bien penser à toujours ajouter votre namespace (`grp-xx`) comme champ `namespace` dans la section [`metadata`](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.13/#objectmeta-v1-meta) de vos fichiers de déploiements yaml.

> D'une manière générale, préférez `kubectl apply` à `kubectl create`, y compris pour la création de nouveaux pod/déploiements, etc.

### Conteneurs stateless: nginx

Suivre [ce tutoriel](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/) afin de déployer un conteneur `nginx`.

> Avant de détruire le déploiement à la fin du tutoriel, vérifiez que le conteneur fonctionne correctement via [`kubectl port-forward`](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/#forward-a-local-port-to-a-port-on-the-pod).

### Conteneurs stateful: postgresql

Adapter le [tutoriel sur le déploiement de MySQL](https://kubernetes.io/docs/tasks/run-application/run-single-instance-stateful-application/) pour:

- utiliser une base PostgreSQL ([image](https://hub.docker.com/_/postgres))
  - attention au point de montage et aux variables d'environnement
- utiliser un volume dont la taille est limitée à **1 Gi**
- ne pas créer de `PersistentVolume`, mais simplement un [`PersistentVolumeClaim`](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.13/#persistentvolumeclaim-v1-core) utilisant:
  - `cinder-storage` comme `storageClassName`
- bien penser à préciser la valeur de la variable `PGDATA` comme indiqué dans [la documentation de l'image docker](https://hub.docker.com/_/postgres).

Supprimer le déploiement, y compris le _Service_ et le _PersistentVolumeClaim_.

> Vous avez un quota de 10Gi de volumes pour votre projet. Si vous oubliez de supprimer les volumes qui ne servent plus, vous atteindrez rapidement cette limite (la taille minimale d'un volume étant de 1Gi).

## Registre privé

### Configuration de votre machine

En complément du cluster kubernetes, un registre docker a été créé: `harbor.tiw1.os.univ-lyon1.fr`.
Ce registre dispose d'une interface Web (https://harbor.tiw1.os.univ-lyon1.fr) qui vous permttra de consulter son contenu.
Le login est votre login étudiant et votre mot de passe est indiqué sur tomuss (`mdp_harbor`).
Sur ce registre, vous avez la possibiliter de gérer des images docker taguées par `harbor.tiw1.os.univ-lyon1.fr/grp-xx/yyyy:zz` o`grp-xx` est le même que votre projet/namespace Rancher.

Pour utiliser ce registre avec docker, il faut d'abord configurer ce dernier ensuivant les instructions indiquées au point 3. ici: https://docs.docker.com/registry/insecure/#use-self-signed-certificates. Le certificat du serveur est disponible à la racine des sources du TP. À défaut, il est également posible de déclarer le nom de domaine `harbor.tiw1.os.univ-lyon1.fr` comme registre non sécurisé, voir ici: https://docs.docker.com/registry/insecure/#deploy-a-plain-http-registry.

Vérifiez que vous pouvez vous connecter au registre via `docker login harbor.tiw1.os.univ-lyon1.fr` en utilisant les mêmes logins et mots de passe que pour l'interface Web de Harbor.
Une fois le login effectué, vous devez pouvoir pousser une image, pour tester (penser à remplacer `grp-xx`):

```
docker pull hello-world
docker tag hello-world:latest harbor.tiw1.os.univ-lyon1.fr/grp-xx/montest:latest
docker push harbor.tiw1.os.univ-lyon1.fr/grp-xx/montest:latest
```

### Utilisation dans kubernetes

Pour que Kubernetes puisse utiliser vos image, il lui faut pouvoir se connecter au registre privé.
Pour cela on va suivre la documentation (https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/) et créer un secret kubernetes (nommé `harborcred`) et contenant les informations de connexion (bien penser à remplacer les valeurs):

```
kubectl create secret docker-registry harborcred --docker-server=harbor.tiw1.os.univ-lyon1.fr --docker-username=pxxxxxxxx --docker-password=XXXXXXXXXX --docker-email=pxxxxxxx@etu.univ-lyon1.fr
```

On va ensuite utiliser ce secret pour lancer un job de test utilisant l'image que l'on a poussé ci-dessus (bien remplacer les valeurs):

```yaml
apiVersion: batch/v1
kind: Job
metadata:
  name: test-harbor-pull
spec:
  template:
    spec:
      containers:
        - name: test-harbor-pull
          image: harbor.tiw1.os.univ-lyon1.fr/grp-xx/montest:latest
      restartPolicy: Never
      imagePullSecrets:
        - name: harborcred
  backoffLimit: 4
```

Vérifier ensuite dans rancher que le job a fonctionné (en sélectionant le projet `grp-xx` du cluster `etudiants` du menu à côté de la vache, dans l'onglet `Workload`)

En cas de problème, des infos sont disponibles en suivantes les liens du job, puis du pod et en afficahnt les événements du pod.

## Déploiement des applications du TP4

Commencer par taguer et pousser les différentes images docker du [TP4 - Services & Communication par messages](../tp4/tp4.md) sur Harbor.

> Si vous n'avez pas réussi à finir le TP4, vous pouvez utiliser les images docker du projet [harbor.tiw1.os.univ-lyon1.fr/tp6-fourni](https://harbor.tiw1.os.univ-lyon1.fr/harbor/projects/30/repositories).

### Application registre

Déployer l'application `registre` dans un premier temps sans configuration particulière (penser tout de même à faire un _Service_ pour aller avec).
Vérifier son fonctionnement (en faisant une requête curl par exemple, comme pour nginx).
Modifier le déploiement de façon à utiliser un fichier d'initialisation:

- créer une ConfigMap ([doc](https://kubernetes.io/docs/concepts/configuration/configmap/)) contenant les info du fichier json d'initialisation
- modifier la configuration du déploiement de façon à injecter la ConfigMap sous forme de volume ([doc](https://kubernetes.io/docs/concepts/configuration/configmap/#using-configmaps-as-files-from-a-pod))
- ajouter le nom du fichier (du point de vue du conteneur) aux arguments du conteneur, en utilisant `args` dans la section `containers` ([doc](https://v1-18.docs.kubernetes.io/docs/reference/generated/kubernetes-api/v1.18/#container-v1-core))

Utiliser `kubectl apply -f monfichier.yaml` pour mettre à jour votre déploiement.

Créer ensuite un déploiement PostgreSQL comme précédement en changeant le nom pour en faire une instance dédiée à l'application `registre`.

Modifier à nouveau le déploiement de `registre` en spécifiant les variables d'environnement adéquates pour utiliser la base postgres qui vient d'être déployée.
Les variables d'environnement se spécifient dans l'entrée `env` des `containers` ([doc](https://v1-18.docs.kubernetes.io/docs/reference/generated/kubernetes-api/v1.18/#container-v1-core)).

> Les variables d'environnement utilisables sont visibles dans le `entrypoint-registre.sh`

> Le hostname de la base postgres est le nom du _Service_ Kubernetes utilisé pour l'exposer

Après la mise à jour du déploiement de `registre`, utiliser `kubectl exec` pour lancer un shell `psql` dans le conteneur de la base et vérifier que les tables ont bien été créées.

### Applications banque

Déployer une instance RabbitMQ.

Reprendre le travail effectué ci-dessus pour créer deux déploiements de banques ayant chacune leur instance postgres dédiée.
Bien penser à changer la configuration de la base de données pour chacune des deux instances.

> Il peut être éventuellement plus facile de procéder en plusieurs étapes, comme avec le registre.

Tester avec votre client Spring Shell en redirigeant les ports avec `kubectl`.

### Application fournisseur

Terminer les déploiements en créant un déploiement avec base de données postgres pour l'application `fournisseur`.
