# Services & Communication par messages

## 1. Objectifs

Ce TP vise à mettre en place un processus métier faisant intervenir
plusieurs services qui vont interagir entre de manière plus complexe
qu'une simple requête client/serveur.

On procédera en plusieurs étapes:

1. Écriture de clients Java afin d'invoquer un service depuis un
   serveur.
2. Implémentation de service SOAP et génération de code. Utilisation
   d'un service registre pour récupérer le point d'accès à un service.
3. Construction d'images Docker et configuration via montage et/ou
   variables d'environnement.
4. Communication par bus de messages.


## 2. Métier

Le métier décrit ci-dessous se veut volontairement simplifié.
De plus on ne se préoccupera pas des problématiques de sécurité ici.

On souhaite augmenter le quota de machines et de connexions
disponibles auprès d'un fournisseur externe.  Le fournisseur sera
représenté par un serveur `fournisseur` qui implémentera:

- un premier point d'accès utilisable par l'organisation qui utilise
  les VMs pour consulter et demander l'augmentation de son quota
- un client RabbitMQ qui surveillera des message provenant de la
  banque du fournisseur afin de confirmer la réception d'un paiement
  et débloquer une augmentation de quota

Les banques seront représentées par un ou plusieurs serveurs `banque`.

- Ces serveurs implémentent une API REST de consultation et de mise
  à jour de compte.
- Ils exposent également un point d'accès pour effectuer des
  demandes de virement.
- Enfin une communication interbancaire sera implémentée via
  l'utilisation de files RabbitMQ.
- Le nom de la banque correspond à la property Spring `tiw1.banque.id`.

Un service `registre` permettra d'obtenir les adresses des points
d'accès des services de banque.  Les serveurs des banques
s'enregistreront à terme auprès de ce service, mais on utilisera un
remplissage du registre par fichier de configuration dans un premier
temps.

### Processus métier

Le principe d'une demande d'augmentation de quota est le suivant:

- l'organisation demande au fournisseur une augmentation de quota qui
  lui répond avec un compte sur lequel faire un virement ainsi qu'une
  référence permettant de valider la demande;
- l'organisation demande à sa banque d'effectuer un virement de son
  compte vers celui du fournisseur en indiquant en particulier la
  référence de validation;
- la banque de l'organisation effectue un transfert d'argent vers la
  banque du fournisseur, avec la référence (ce transfert nécessite de
  récupérer les adresses de la banque destination et de vérifier
  l'existence du compte destination);
- la banque du fournisseur prévient celui-ci que le virement a été
  effectué en lui transmettant la référence de la demande de quota.


L'interaction à laquelle on parviendra à la fin de ce TP est donnée
par le diagramme suivant:

```mermaid
sequenceDiagram
    participant O as Organisation
    participant F as Fournisseur
    participant B1 as Banque1
    participant B2 as Banque2
    participant R as Registre
    O->>F: SOAP: M1
    F-->>O: R1
    O->>B1: SOAP: M2
    activate B1
    B1->>R: REST: M3
    R-->>B1: R3
    B1->>B2: REST: M4
    B2-->>B1: R4
    B1->>R: REST: M5
    R-->>B1: R5
    B1-xB2: RabbitMQ: M6
    activate B2
    B1-->>O: R2
    deactivate B1
    B2-xF: RabbitMQ: M7
    deactivate B2
```

- M1: demande aug. quota (org, quota-additionnel)
  - R1: (montant, compte-dest, ref)
- M2: virement (compte-src, compte-dest, montant, ref)
  - R2: OK ou erreur de compte inconnu
- M3: adresse? (banque2)
  - R3: (adresse REST pour les comptes, adresse RabbitMQ pour le transfert)
- M4: compte-dest ?
  - R4: info (-> le compte existe) ou 404 (il n'existe pas)
- M5: adresse ? (banque2)
  - R5: (adresse REST pour les comptes, adresse RabbitMQ pour le transfert)
- M6: virement (compte dest, montant, ref)
- M7: confirmation de virement (montant, ref)

## 3. Clients

### 3.1 Spring RestTemplate

Lire le tutoriel suivant sur `RestTemplate`:
https://www.baeldung.com/rest-template .  Utiliser `RestTemplate` pour
compléter les méthodes `banqueEndpoint` et `banqueQueue` de la classe
`BanqueExterneService` de l'application `banque`.

L'application `registre` peut être démarrée en passant en ligne de
commande un fichier JSON contenant une liste de banques avec leurs
points d'accès (voir les classes `Banque` et `RegistreInitLoader` de
l'application `registre`).  Il peut également être intéressant de
changer le port de cette application, par exemple via la propriété
`server.port` (en la fixant dans `application.properties`).

Tester votre code client en ayant pris soin de démarrer le registre
auparavant.  À ce stade, il est probablement nécessaire d'exclure ce
test des tests unitaires de la banque (par exemple en y ajoutant des
tags et en filtrant lors de l'exécution des tests, comme suggéré ici:
https://keyholesoftware.com/2018/02/12/disabling-filtering-tests-junit-5/).

Pour exclure des tests par tags dans les tests exécutés via maven, on pourra ajouter les configuration suivantes (chemin dans le `pom.xml` indiqué en XPath):

Dans `/project`:

```xml
    <profiles>
        <profile>
            <id>integration</id>
            <properties>
                <excludeTags>none</excludeTags>
            </properties>
        </profile>
        <profile>
            <id>unit</id>
            <properties>
                <excludeTags>integration</excludeTags>
            </properties>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
        </profile>
    </profiles>
```

Dans `/project/build/plugins`:

```xml
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <properties>
                        <excludeTags>${excludeTags}</excludeTags>
                    </properties>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.platform</groupId>
                        <artifactId>junit-platform-surefire-provider</artifactId>
                        <version>1.3.2</version>
                    </dependency>
                </dependencies>
            </plugin>
```

### 3.2 API SAAJ

Créer une nouvelle application Spring-Boot, sans Spring Web MVC
(i.e. on ne veut pas faire un serveur Web), mais avec [Spring
Shell](https://projects.spring.io/spring-shell/) ([doc](https://spring.io/projects/spring-shell#learn)).
Dans cette application, créer un client pour le service SOAP de
`banque` en utilisant l'API SAAJ.  Le WSDL de ce service se trouve
ici: http://localhost:8080/ws/banque.wsdl, une fois l'application
`banque` démarrée.  On testera pour le moment avec un transfert vers
un compte interne.

> Pour tester avec JUnit: 
> Annoter la classe de test comme suit:
> 
> ```java
> @SpringBootTest(properties = {
>         InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
>         ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT + ".enabled=false"
> })
> ```
> 
> Cela désactivera le lancement du shell interactif lors des tests sur cette classe.
> 
> Ref: https://github.com/spring-projects/spring-shell/issues/204

> L'utilisation de SAAJ nécessite `saaj-impl`. Comme pour les questions suivantes vous aurez besoin de `spring-boot-starter-web-services` pour faire le client via `WebServiceTemplate`, utilisez le en dépendance dès maintenant.
> Il faut par contre désactiver le tomcat qui vient avec (dans l'élément `/project/dependencies` du `pom.xml` de votre client):
>
> ```xml
> 		<dependency>
> 			<groupId>org.springframework.boot</groupId>
> 			<artifactId>spring-boot-starter-web-services</artifactId>
> 			<exclusions>
> 				<exclusion>
> 					<groupId>org.springframework.boot</groupId>
> 					<artifactId>spring-boot-starter-tomcat</artifactId>
> 				</exclusion>
> 			</exclusions>
> 		</dependency>
> ```

## 4. Points d'accès SOAP

### 4.1 Service SOAP

En vous inspirant de la configuration du point d'accès implémenté dans
`banque`, implémenter un point d'accès SOAP dans l'application
`fournisseur`. Ce point d'accès aura une opérations pour faire une
demande d'augmentation de quota. Il faudra en particulier:

- Ajouter la dépendance maven vers `wsdl4j` oubliée dans le `pom.xml` fourni.
- Changer au besoin le port via `server.port` dans `application.properties`.
- Créer une entité `Demande` pour stocker les demandes en base.
- Créer un fichier `.xsd` pour contenir le schéma de l'opération de demande d'augmentation et configurer le plugin de génération de code JAXB.
- Créer un service Spring qui implémente le métier (par exemple qui calcule le montant avec des règles que vous aurez choisies)
- Créer le _endpoint_ qui fait appel au service Spring de manière similaire à `CompteEndpoint`.
- Exposer cet _endpoint_ via un `WsConfigurerAdapter` similaire au `WebServiceConfig` de `banque`.
- Il peut être pratique d'ajouter un `RestController` pour afficher des demandes via un `GET`.

### 4.2 Client SOAP via WebServiceTemplate

Créer ensuite un client dans l'application Spring-Shell créée en 3.2.
Pour réaliser ce client, on utilisera les `WebServiceTemplates` de
Spring:

- [doc](https://docs.spring.io/spring-ws/site/reference/html/client.html)
  assez complète mais pas forcément à lire en premier;
- [tutoriel](https://spring.io/guides/gs/consuming-web-service/), les
  extraits de code sont cassés, mais ont peut les trouver
  [ici](https://github.com/spring-guides/gs-consuming-web-service), en
  particulier
  [`CountryClient.java`](https://github.com/spring-guides/gs-consuming-web-service/blob/master/complete/src/main/java/com/example/consumingwebservice/CountryClient.java)
  et
  [`CountryConfiguration.java`](https://github.com/spring-guides/gs-consuming-web-service/blob/master/complete/src/main/java/com/example/consumingwebservice/CountryConfiguration.java));
- il faudra récupérer le `.xsd` que vous aurez créé pour implémenter
  le service `fournisseur` afin de générer les classes représentant
  les payload des messages.
- sous java 11+ ajouter la dépendance (dans l'élément `/project/dependencies` du `pom.xml`du client spring-shell):
  ```xml
      <dependency>
        <groupId>org.glassfish.jaxb</groupId>
        <artifactId>jaxb-runtime</artifactId>
      </dependency>
  ```


## 5. Docker build & compose

### 5.1 Docker build & run

On va maintenant s'intéresser au déploiement des différentes applications via des conteneurs.

La première étape consiste à construire des images simples.
Un exemple de `Dockerfile` et de script de construction (`build.sh`) est fourni pour l'application `registre`.
Répliquer cette construction pour les applications `banque` et `fournisseur`.

Une fois les images construites, lancer ces applications en prenant soin de rediriger les ports ([doc](https://docs.docker.com/engine/reference/run/#expose-incoming-ports)) pour y accéder depuis les applications que vous lancerez à la main. Par exemple:

```
docker run --name registre -p 8081:8081 tiw1/registre:0.0.1 java -jar /app/registre-0.0.1-SNAPSHOT.jar
```

> Pour le moment, on utilisera pas de base de données PostgreSQL externe, mais des bases H2 en mémoire. Vérifiez et modifiez au besoin vos configurations de bases de données dans vos `application.properties`.

Vérifiez que vous pouvez accéder aux services comme lorsque vous les lancez depuis l'IDE ou la ligne de commande.

### 5.2 Entrypoints

Avant de créer une configuration globale de lancement (via `docker-compose` en 5.3) pour les différentes applications, on souhaite rendre l'exécution des ces applications plus facilement configurable.
Pour cela on va introduire des propriétés de configuration pour les différentes adresses de services connues au démarrage (par exemple l'adresse du `registre`).
On utilisera l'annotation `@Value` (cf sect. 3 de cette [mini-doc](https://www.baeldung.com/properties-with-spring)) pour injecter les valeur de configuration dans les beans qui en ont besoin.
Une fois l'application configurable, il faut pouvoir transmettre cette configuration lors du lancement des conteneurs docker.
On va pour cela changer l'_entrypoint_ du conteneur, c'est à dire la commande lancée par docker et qui va interpréter les arguments qui lui sont au conteneur.

On souhaite donc écrire un script (typiquement un script shell, mais vous êtes libres ici) qui va pouvoir lancer l'application spring-boot avec les bonnes options.
En particulier on souhaite changer la configuration de l'application via des variables d'environnement. Celles-ci seraient interprétées par ce script pour ajouter les bons arguments en ligne de commande au lancement via spring-boot, typiquement pour modifier les valeurs de configuration comme expliquer en section 4.8 de la [mini-doc](https://www.baeldung.com/properties-with-spring)).

Une fois le script créé pour une des applications, modifier son `Dockerfile` pour copier le script dans l'image et changer l'entrypoint en indiquant le script en question.
Répéter cette opération pour les 3 applications `registre`, `banque`, et `fournisseur`.

### 5.3 Docker-compose

Compléter le fichier `docker-compose.yaml` pour lancer un registre, un fournisseur et deux banques.
Configurer chaque application pour qu'elles se connaissent (e.g. les banques doivent connaitre le registre) en spécifiant les bonnes valeurs pour leurs variables d'environnement.

> À noter que dans les urls à utiliser le nom de domaine d'un conteneur est le nom du service, par exemple si le `service` de la première banque s'appelle `banque1`, l'url pour accéder à ses compte depuis les autres conteneurs sera `http://banque1:8080/compte`.

> Pensez à rediriger les ports des conteneurs (comme cela a été fait pour postgres) afin de pouvoir vous y connecter à la main pour vérifier que tout fonctionne.

## 6. Files de message RabbitMQ


On souhaite maintenant terminer l'implémentation du processus métier décrit dans le diagramme de séquence en implémentant la communication inter-banque et la communication banque-fournisseur via RabbitMQ.

### 6.1 Communication interbancaire

> Ajouter les dépendances suivantes dans le `pom.xml` de la banque:
>
> ```xml
> <dependency>
>   <groupId>org.springframework.boot</groupId>
>   <artifactId>spring-boot-starter-amqp</artifactId>
> </dependency>
> <dependency>
>   <groupId>org.springframework.amqp</groupId>
>   <artifactId>spring-rabbit-test</artifactId>
>   <scope>test</scope>
> </dependency>
> ```

Ajouter un serveur `rabbitmq` au `docker-compose.yaml`. 
Utilisez de préférence l'image `rabbitmq:3-management` afin de bénéficier d'une interface web de monitoring sur le port `15672`.
Ce serveur n'a normalement pas besoin d'être configuré pour être opérationnel.

Créer un bean pour écouter les demandes de transfert parvenant sur une file RabbitMQ et les traiter en ajoutant une méthode `transfert` à `CompteService` qui créditera le compte cible.

> Dans le `application.properties` / entrypoint penser à spécifier `spring.rabbitmq.host` ([doc](https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html#integration-properties)) pour indiquer le nom du serveur rabbitmq.

> Pensez que vous devez décoder les informations transmises sur la file.
> Vous êtes libres du format et de la bibliothèque de sérialisation: vous pouvez par exemple json avec Jackson ou encore XML avec JAXB.

> Utilisez le paramètre `queuesToDeclare` de `@RabbitListener` ([doc](https://docs.spring.io/spring-amqp/docs/current/api/org/springframework/amqp/rabbit/annotation/RabbitListener.html#queuesToDeclare--)) afin d'éviter d'avoir à configurer trop de choses dans la configuration RabbitMQ.
> Vous pouvez utiliser les expressions Spring (`${...}`) pour indiquer un nom de queue prise dans la configuration.
> Exemple: `@RabbitListener(queuesToDeclare = @Queue(name = "${...}"))`  ([doc](https://docs.spring.io/spring-amqp/docs/current/reference/html/#async-annotation-driven), chercher `queuesToDeclare`).

Utiliser ensuite un `RabbitTemplate` afin de compléter la méthode `transfert` de `BanqueExterneService`.

À ce stade, il peut être intéressant d'ajouter un composant à la banque qui lui permettra de s'auto-enregistrer auprès du registre.
Pour cela, on peut suivre la démarche de la section 2.3 de cette [mini doc sur "setup logic"](https://www.baeldung.com/running-setup-logic-on-startup-in-spring).

> Ne passez pas trop de temps sur l'auto-enregistrement, vous pourrez y revenir plus tard. En attendant vous pouvez tout faire avec une configuration statique ou en faisant des requêtes curl/postman/jmeter à la main sur le registre.

Tester en utilisant le client spring-shell, puis en affichant le compte destination.
Il faudra pour cela bien avoir redirigé les port dans le docker-compose afin de pouvoir accéder aux deux banques.
Il est également possible d'écrire un test d'intégration pour vérifier le bon fonctionnement du transfert.


### 6.2 Terminer le processus métier d'augmentation de quota

Terminer le processus métier:

- ajouter un nom de queue aux comptes bancaires
- envoyer la référence sur cette queue lors de la réception d'un transfert
- valider l'augmentation de quota sur réception du bon montant lors de la réception de l'information de transfert

Tester en utilisant le client Spring-Shell et des requêtes curl pour vérifier l'état des comptes et des demandes.
Idéalement, faites un test d'intégration qui vérifie le processus métier complet.


