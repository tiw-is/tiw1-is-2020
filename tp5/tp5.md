Université Claude Bernard Lyon 1 – M2 TIW – Intergiciels et Services

# TP OSGi

## Objectifs pédagogiques

Expérimenter une architecture à base de composants téléchargeables dynamiquement.

## Outils

Dans ce TP, vous utiliserez le framework [Felix](http://felix.apache.org/documentation/subprojects/apache-felix-ipojo.html), qui contient une implémentation minimale d'OSGi et une console ("gogo" shell) en ligne de commande.
Un début de documentation de ce shell est disponible [ici](https://felix.apache.org/documentation/subprojects/apache-felix-framework/apache-felix-framework-usage-documentation.html#framework-shell).

## Découverte du framework Felix

Téléchargez et décompressez le framework. Vous pouvez ouvrir un shell en exécutant, depuis le répertoire principal du framework, la commande `java -jar bin/felix.jar`.

Utiliser la documentation située [ici](https://felix.apache.org/documentation/subprojects/apache-felix-framework/apache-felix-framework-usage-documentation.html#framework-shell) pour démarrer. Par exemple, taper `lb` (list bundles) pour avoir la liste des bundles installés par défaut.

### Chargement et exécution d'un bundle

Explorer le [Repository Felix OBR](http://felix.apache.org/downloads.cgi). Choisir et télécharger un ou plusieurs bundle(s), installer, lancer et stopper ce(s) bundle(s) comme indiqué [là](http://felix.apache.org/site/apache-felix-framework-usage-documentation.html#ApacheFelixFrameworkUsageDocumentation-installingbundles). Normalement, vous devez voir s'exécuter les méthodes de gestion du cycle de vie `start()` et `stop()`, d'implémentation de l'interface `BundleActivator`.

Dans la suite, vous pourrez avoir besoin de recourir à ce repo pour récupérer et déployer des bundles nécessaires à l'exécution de votre application.

### Réalisation d'un bundle avec iPOJO

Vous allez maintenant réaliser vous-même un bundle OSGi, en suivant le tutoriel situé [ici](https://felix.apache.org/documentation/subprojects/apache-felix-ipojo/apache-felix-ipojo-gettingstarted/ipojo-hello-word-maven-based-tutorial.html).

- Une fois les fichiers recopiés et les jars faits, suivre la même procédure que précédemment pour les déployer et les démarrer.
  Vous devrez démarrer les bundles suivants (dans "annotations/hello.felix.annotations/target/bundle") : `org.apache.felix.ipojo-1.12.1`, `hello.service-1.12.1`, `hello.impl.annotations-1.12.1` et `hello.client.annotations-1.12.1`.
- Alternativement, partez du code téléchargeable au début du tuto, puis suivez les instructions pour compiler avec Maven et lancer félix dans le répertoire `annotations/hello.felix.annotations/target`.

## Client HTTP simple

En suivant l'un des exemples précédents (bundle "classique" ou iPOJO), créez un bundle OSGI capable d'interroger un serveur HTTP (n'importe quel nginx, Tomcat ou projet Spring que vous avez sous la main fera l'affaire ; évitez les services externes pour ne pas qu'on se fasse blacklister...).
En première approximation, ce client n'aura besoin que d'être capable d'envoyer périodiquement (toutes les 5 secondes) une requête HTTP et de recevoir une réponse, quel qu'en soit le code de retour, puis de loguer l'URL de la requête et le code de réponse sur la console.

> Ce bundle dépendra du module [HttpClient d'Apache](https://hc.apache.org/httpcomponents-client-ga/) :
>
> - [Documentation du bundle](https://hc.apache.org/httpcomponents-client-ga/httpclient-osgi/apidocs/)
> - [Dépendance Maven](https://hc.apache.org/httpcomponents-client-ga/httpclient-osgi/dependency-info.html)

## Client dynamique

"Découpez" maintenant votre client en 3 parties :
  1. un bundle qui contient une classe de configuration qui renvoie uniquement l'URL du serveur HTTP
  2. un bundle qui contient l'interface de cette classe de configuration (et qu'implémente le précédent)
  3. un bundle qui contient tout le reste du client (et qui dépend du précédent)

Démarrez ces bundles et testez votre client.

Démarrez un nouveau serveur sur un autre port de votre machine, et créez le bundle 1bis qui contient sa nouvelle URL, et qui s'enregistre sous le même nom que le bundle 1. Arrêtez les bundles 1 et 3, démarrez le 1bis, puis le 3. Vous devez maintenant pourvoir accéder au nouveau serveur.

Maintenant, faites en sorte que le bundle 3 s'abonne aux changements d'implémentation de l'interface 2, comme indiqué dans le cours. Vous devez pouvoir démarrer alternativement les bundles 1 et 1bis, et changer ainsi de serveur sans redémarrer votre client.

> Vous avez réalisé un bundle capable de s'adapter dynamiquement ("à chaud") aux changements d'implémentations de l'interface dont il dépend.

## Reprise de l'application de gestion de sessions RDP

S'il vous reste du temps, implémentez, sur le même modèle, un client plus complet du service de gestion de sessions à partir de votre implémentation, ou de celle disponible dans le [projet Spring fourni](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2020-sources/-/tree/master/gestion-rdp-spring).