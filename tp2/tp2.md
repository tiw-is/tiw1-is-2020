Université Claude Bernard Lyon 1 – M2 TIW – Intergiciels et Services

# TP 2 : Conteneurs d'objets et inversion de contrôle

N'oubliez pas de faire une branche git `tp2` dans laquelle vous ferez toutes les modifications de ce TP.

## Objectifs pédagogiques

- Mettre en pratique différents patterns (IoC, Contexte, Annuaire, Object pool...) afin de mieux comprendre le fonctionnement d'un framework.
- Mettre en place un outil configurable et capable de gérer le cycle de vie de ses composants.

Dans ce TP, vous allez transformer une application standalone en application côté serveur, et petit à petit, construire les différents éléments d'un framework autour d'elle.

## Description du métier de l'application

Dans le cadre de la mise en place raisonnée du télétravail pour ses employés, votre entreprise de e-commerce vous charge de concevoir une application de gestion de sessions Remote Desktop Protocol ([RDP][rdp]) sur des machines (virtuelles ou [_baremetal_][baremetal]) où sont déployés les différents processus métier de l’entreprise (gestion des stocks, des ventes…).

Votre entreprise loue des ressources chez plusieurs fournisseurs (clouds public ou data centers propres à l'organisation). Les fournisseurs allouent un nombre limité (i.e. quota) de sessions simultanées à leurs clients (mais pas par VM).

Les utilisateurs utilisent ces sessions RDP pour travailler à distance sur des VMs pendant un temps donné. Ils se connectent à n'importe quelle VM de manière transparente, à l'aide de leurs navigateurs, via un serveur déployé par votre application qui leur masque l'infrastructure du fournisseur. Sur votre application, un utilisateur peut ouvrir et fermer une session, et consulter l'ensemble des sessions disponibles chez les différents fournisseurs à un instant donné.

## Code fourni : système de gestion de sessions RDP

### Description du projet

- Le contenu qui vous a été donné dans le projet `gestion-rdp` se trouvera côté serveur. Pour vous faciliter les choses, le TP est autonome et découplé du TP1. Vous y retrouverez donc des mocks des objets et services métier du TP1. Il est demandé de ne pas en modifier le fonctionnement.
  **Vous devez avoir démarré le serveur Postgres avant de lancer le code côté serveur.**
- Dans ce TP, le client sera simulé par les tests. Certains sont déjà écrits, mais vous les ferez évoluer avec l'application. Ces tests adresseront directement le serveur et non les classes métier.

Dans ce TP, vous aurez 3 types de données à manipuler avec chacune une durée de vie différente :

1. Les fournisseurs (et leurs quotas), qui sont supposées exister et ne pas varier tout au long de ce TP (on ne s'intéresse pas aux mises à jour ni à la reconfiguration de l'application) ; ces données seront insérées "en dur" dans la BD
2. Les connexions ouvertes entre votre application et les infrastructures des fournisseurs : ce sont elles qui véhiculent les sessions ; toute session active nécessite une connexion, laquelle est facturée par le fournisseur à votre société ; elles sont créées au besoin et dans la limite des quotas alloués par votre application
3. Les sessions ouvertes sur les machines, qui peuvent être créées et relâchées à tout moment ; la durée de vie d'une session est déterminée par les opérations d'ouverture et d'arrêt, et est de toutes façons inférieure à celle de la connexion qu'elle utilise

Les § ci-dessous décrivent le métier du projet fourni par rapport à celui du TP1.

### Objets métier

- Nouveaux objets métier gérés par l’application :
  - Connexion au fournisseur de machines
  - Session RDP
- Autres objets métier (normalement gérés par l'application du TP1, mocks fournis pour découpler les TPs) :
  - Fournisseur
  - Machine

### Nouvelles opérations métier (Use cases)

**Ouvrir une session sur une VM**

Précondition : L'utilisateur est authentifié sur votre application (aka le système) et connaît l'IP de la VM à laquelle il veut se connecter

1. L'utilisateur demande au système une session sur la VM correspondant à une IP donnée
2. Le système identifie le fournisseur de la VM à partir de cette IP
3. Le système vérifie qu’il reste des sessions disponibles dans le quota du fournisseur<br>
   3bis. Echec : le système informe l’utilisateur que le quota est dépassé et qu’il faut qu’il attende qu’un autre utilisateur relâche sa session
4. Le système ouvre une connexion sur la VM demandée
5. Le système incrémente le nombre de sessions ouvertes chez le fournisseur
6. Le système crée une nouvelle session qui utilise cette connexion pour accéder à la VM
7. Le système rend cette session accessible à l'utilisateur
8. Le système transmet à l'utilisateur une représentation de sa session, comportant notamment un UUID (ou une URL) lui permettant d'y accéder
9. L'utilisateur accède à la session

**Arrêter une session sur une VM**

Précondition : l'utilisateur est authentifié et est en train d'utiliser une session sur une machine distante

1. L’utilisateur indique au système qu'il souhaite fermer sa session sur une VM<br>
   1bis. Alternative : au bout d’un certain temps d’inactivité, le système ferme automatiquement la session de l’utilisateur
2. Le système ferme la connexion liée à la session
3. Le système décrémente le nombre de sessions ouvertes chez le fournisseur

**Consulter les sessions disponibles chez tous les fournisseurs**

Précondition : l'utilisateur est authentifié

1. Pour chaque fournisseur, le système récupère le quota du fournisseur
2. Pour chaque fournisseur, le système récupère le nombre de sessions actuellement ouvertes par tous les utilisateurs
3. Pour chaque fournisseur, le système calcule et présente à l'utilisateur le nombres de connexions disponibles

## 0. Premières manipulations

Créez un package `serveur` dans lequel vous placerez une classe `Serveur`. Cette classe interfacera les clients avec l'ensemble des objets de l'application. Dans un premier temps, cette classe aura pour rôle :

- d'instancier les objets persistants (fournisseurs) et ceux qui seront passés à la création des connexions et des sessions,
- de répondre aux requêtes (opérations métier)

Faites en sorte que votre serveur expose (dans des méthodes publiques) les opérations décrites plus haut :

- Ouverture de session
- Fermeture de session
- Consultation du nombre de connexions disponibles

Les échanges de données entre client et serveur se feront en JSON.

### Autres manipulations

- Utilisez le pattern DTO pour simplifier l'interface du serveur et permettre de récupérer et de relâcher facilement des sessions.
- Créez un objet destiné à sérialiser les résultats de la consultation des connexions disponibles. Vous pouvez y ajouter des statistiques supplémentaires (temps moyen de connexion...)
- Mettez en place un pattern MVC très simple :
  - le modèle sera celui des classes métier du code fourni
  - la vue sera une sérialisation en JSON des objets renvoyés par ces méthodes
  - créez un contrôleur auquel le serveur délèguera l'appel des méthodes du modèle
- Bonus : implémentez le timeout de fermeture automatique de session.

Testez.

> À ce stade, vous devez avoir un serveur qui héberge une application codée en MVC, laquelle expose ses méthodes métier.

## 1. Inversion de contrôle

Vous allez maintenant mettre en place un conteneur et remplacer toute la logique d'injection de dépendances "directe" que vous avez créée à la section précédente par un mécanisme de résolution de dépendances. Pour cela :

- Créez un conteneur (voir indications ci-dessous)
- Placez le contrôleur et tous les objets dont il a besoin pour s'instancier dans ce conteneur.

### Indications

- Conteneur :
  La classe Serveur instanciera un conteneur qui sera basé sur l'outil [PicoContainer](http://picocontainer.com/).
  Alternativement :

  - Solution à préférer : vous ajouterez la [dépendance Maven sur la version 2.15 de Picocontainer](https://search.maven.org/artifact/org.picocontainer/picocontainer/2.15/jar)
  - Défaut : vous téléchargerez la dernière version du jar [sur le site](http://central.maven.org/maven2/org/picocontainer/picocontainer/2.15/picocontainer-2.15.jar) ou la version disponible en local [ici](https://perso.liris.cnrs.fr/lionel.medini/enseignement/IS/TP1/picocontainer-2.15.jar.)

  La doc est disponible [sur le site](http://picocontainer.com/) et une Javadoc de la version 2.14.3 [en local](https://perso.liris.cnrs.fr/lionel.medini/enseignement/CAHD/TP1/picocontainer-2.14.3-javadoc.jar). Commencez par lire la première page d'introduction à l'utilisation de cet outil, située [ici](http://picocontainer.com/introduction.html).

- Arbre de dépendances :

  ```
  Controleur -> String contenant le nom de votre organisation
  Controleur -> GestionRDP

  GestionRDP -> VMDAO
  GestionRDP -> SessionService
  GestionRDP -> FournisseurDAO

  SessionService -> SessionDAO
  SessionService -> ConnexionService

  ConnexionService -> ConnexionDAO

  SessionDAO -> DBAccess
  SessionDAO -> ConnexionDAO
  SessionDAO -> VMDAO

  ConnexionDAO -> DBAccess
  ConnexionDAO -> FournisseurDAO

  FournisseurDAO -> DBAccess

  DBAccess -> String (url de connexion)
  DBAccess -> String (db username)
  DBAccess -> String (db password)
  ```

[Désambiguïsez](http://picocontainer.com/disambiguation.html) les noms dans les paramètres des constructeurs pour que PicoContainer soit capable de résoudre le référentiel de dépendances.

- Méthodes de la classe `Controleur` :

  - méthodes de gestion du cycle de vie : faites en sorte que la classe `Controleur` implémente l'interface Startable et ajoutez-y les méthodes requises ; dans la méthode `start()`, rajoutez un affichage indiquant que le serveur a démarré, le type de classe d'implémentation et l'instance du `Controleur` (aide : utilisez l'API Reflection / la méthode toString() de l'instance). L'objectif est d'obtenir un affichage du style :

  `Composant Controleur démarré : fr.univlyon1.m2tiw.tiw1.metier.contoleur.Controller@95c083`

- Méthodes de la classe `Serveur` :
  - constructeur : il instanciera un `DefaultPicoContainer`, puis y rajoutera les composants avec des dépendances entre eux. Il récupèrera ensuite le composant `Controleur` instancié par le conteneur, le stockera dans une variable globale et appellera sa méthode start().
  - méthode (provisoire) de service : getControleur(), renvoyant au client une référence vers l'instance de `Controleur`.

> À ce stade, vous avez inversé le contrôle de vos objets métier en les plaçant dans un serveur (i.e. un framework) qui se charge d'instancier, de gérer et d'utiliser ces objets.

Ne vous préoccupez pas pour l'instant des méthodes permettant de gérer les sessions ou les connexions.

## 2. Isolation et uniformisation des objets côté serveur

**Manipulations préliminaires** :
- le contrôleur et `GestionRDP` remplissent le même rôle dans l'application. Si vous ne l'avez pas fait avant, placez les méthodes de `GestionRDP` dans `Controleur` et **supprimez la classe GestionRDP**.

"Raccourcissez" par conséquent le référentiel de dépendances dans le conteneur : `Controleur` dépend maintenant directement de `String`, `VMDAO`, `SessionService` et `FournisseurDAO`.

### 2.1. Isolation

Bien entendu, vous ne pouvez pas laisser le client accéder directement à l'instance du contrôleur créée dans le conteneur. Pour éviter cela, vous allez implémenter le paradigme requête-réponse :

- Modifiez la méthode de service du serveur pour qu'elle soit plus générique ; par exemple :

  `public Object processRequest(String commande, Map<String, Object> parametres);`

  où les éléments de la Map représentent les paires nom / valeur des paramètres des requêtes

- Dans le contrôleur, passez les méthodes add, remove, get... en privé, et créez une méthode publique `process()`, qui appellera chacune de ces trois méthodes en fonction de la commande
- Modifiez vos tests pour qu'ils n'appellent plus que la méthode `processRequest()` du serveur
- Renommez la classe `Serveur` en `ServeurImpl`, extrayez l'interface de service du serveur (que vous appellerez `Serveur`) et faites en sorte que les clients (les tests) ne connaissent plus que cette interface.

> Remarquez que la classe `ServeurImpl` masque désormais complètement l'implémentation du traitement des requêtes par les objets métier. Il suffit au client de connaître son API pour utiliser l'application.

### 2.2. Uniformisation

Dans cette partie, vous allez modifier le référentiel de dépendances pour vous rapprocher d'un fonctionnement en MVC de type 2 (contrôleurs délégués).

Plutôt que d'avoir un objet `Controleur` qui répond à différentes requêtes, vous allez créer plusieurs objets sur le même modèle, mais traitant chacun un type de requête spécifique (i.e. des contrôleurs délégués / de cas d'utilisation / de ressources). Pour cela :

- Commencez par définir une interface et une classe abstraite reprenant les principales caractéristiques des composants : dépendances, implémentation de `Startable`
- Créez les classes contrôleur qui correspondent au découpage que vous avez choisi et faites en sorte qu'elles implémentent une méthode de service `process()`
- Créez les classes implémentant le modèle et correspondant aux méthodes de service `openSession()`, `closeSession()` et `getConnexionStatistics()`. Vous regrouperez les fonctionnalités dans des classes représentant des ressources (et non des opérations) liées aux objets métier, par exemple une classe `SessionResource` pour l'ouverture et la fermeture de sessions et une classe `StatisticsResource` pour la récupération des statistiques. Pour cela, il faut que vous ajoutiez un paramètre supplémentaire (la notion de "méthode"), qui indique l'opération à réaliser sur la ressource.
- Modifiez le serveur pour que votre conteneur crée les composants correspondant aux instances de vos nouvelles classes
- Au niveau du serveur, créez une méthode d' "aiguillage" des requêtes vers les instances de chacun des contrôleurs, qui sera appelée par la méthode de service du serveur : la commande correspond au nom de la classe à appeler, comme un nom de ressource sur un serveur Web. Créez ensuite (et factorisez dans la classe abstraite), le mécanisme qui appellera la bonne méthode de la classe, en fonction de la valeur du paramètre "méthode" défini plus haut.

**Normalement, votre application ne doit pas fonctionner :** le container vous renvoie une liste vide à chaque opération et les instances des DAO sont différentes dans les messages d'initialisation des méthodes de gestion du cycle de vie.

Cela vient du fait que bien que les SessionDao et ConnexionDao, etc. soient des dépendances communes de tous les `XxxRessource` du conteneur, par défaut, celui-ci résout les dépendances en instanciant un objet différent pour chaque instance de `XxxRessource`. Toutefois, vous pouvez indiquer que vous souhaitez procéder autrement, c'est-à-dire qu'il "cache" les instances. Vous pouvez résoudre ce problème à deux niveaux :

1. Au niveau du composant : en spécifiant la caractéristique "Cache" des composants que vous voulez cacher. Le plus simple est d'utiliser la méthode `as()` du conteneur, comme spécifié [ici](http://picocontainer.com/properties.html).
2. Au niveau du conteneur : en spécifiant un [comportement](http://picocontainer.com/behaviors.html) global de type "Caching" pour tous les composants du conteneur dans le constructeur de celui-ci.

Si vous choisissez la seconde solution, les ressources seront cachées également, et vous n'aurez plus besoin de les stocker dans une variable globale. Par ailleurs, comme indiqué dans le warning du début de la [page sur la gestion du cycle de vie des composants](http://picocontainer.com/lifecycle.html), les méthodes `start()`, `stop()`, etc. sont conçues pour fonctionner avec des composants cachés, et vous pourrez appelez directement la méthode `start()` du conteneur pour qu'il démarre tous les composants qui implémentent `Startable` en même temps...

> À ce stade, vous avez réalisé un outil équivalent à un conteneur de servlets. Il pourra fonctionner avec n'importe quel type de ressource (au sens ReST) utilisé par l'application, effectuer différentes opérations (correspondant à des méthodes HTTP), à partir du moment où celle-ci est déclarée dans le serveur et correspond à une commande reconnue.

## 3. Création d'un contexte applicatif

Dans cette partie, vous allez rajouter un niveau d'indirection entre le conteneur (et ses composants) et leurs dépendances (par exemple, les objets de type DAO). Pour cela, vous allez implémenter une classe qui permettra à chaque instance de `XxxRessource` créée d'accéder au DAO en respectant le pattern Context présenté en cours. Cela permettra par exemple d'utiliser un DAO instancié à l'extérieur du serveur (cas d'une connexion à un SGBD), ou de modifier par configuration le DAO utilisé pour gérer la persistence des données de l'application.

**Attention** : sauvegardez (pushez) l'état actuel de votre application, car dans la suite, vous allez modifier temporairement le référentiel de dépendances. Pour se synchroniser, voici le référentiel de dépendances que vous devriez avoir à ce stade :

  ```
  SessionResource (aka SessionControleur) -> SessionService

  StatisticsResource (aka StatisticsControleur) -> StatisticsService

  SessionService -> SessionDAO
  SessionService -> ConnexionService

  StatisticsService -> VMDAO
  StatisticsService -> FournisseurDAO

  ConnexionService -> ConnexionDAO

  SessionDAO -> DBAccess
  SessionDAO -> ConnexionDAO
  SessionDAO -> VMDAO

  ConnexionDAO -> DBAccess
  ConnexionDAO -> FournisseurDAO

  DBAccess -> String (url de connexion)
  DBAccess -> String (db username)
  DBAccess -> String (db password)

  rien -> String contenant le nom de votre organisation // à conserver pour la question 4.2
  ```


### 3.1. Création du contexte

Créez une interface `SessionServiceContext`, de façon à ce que :

- le service dédié aux opérations sur la session puisse découvrir les références à tous les objets dont il dépend
- ces références soient injectées par le conteneur

En clair, il s'agit d'un objet qui stocke des références vers 1 DAO et 1 service et possède deux accesseurs sur ce champ.

Créez une classe d'implémentation `SessionServiceContextImpl` de cette interface, capable de stocker des références vers les objets dont dépend le service et possèdant des accesseurs et des mutateurs sur ces champs.

Instanciez-la dans votre serveur et ajoutez l'instance créée en tant que composant de votre conteneur.
L'intérêt de cette manipulation est que le serveur connaisse aussi le contexte et puisse interagir avec lui.

### 3.2. Modification de l'arbre de dépendances

Vous allez donc modifier le composant `SessionService` pour qu'il dépende d'une implémentation de `SessionServiceContext` accessible par le conteneur.

**Remarque** : tant qu'à faire, référez-vous à la documentation de PicoContainer pour que l'injection de dépendances dans `SessionService` soit faite par setters (et non par constructeur).

- Modifiez la classe `SessionService` de façon à ce qu'elle ne dépende que d'un `SessionServiceContext` et de rien d'autre.
- Dans le service, récupérez les dépendances "réelles" par des appels aux getters correspondants du contexte. Placez cette opération dans une méthode de gestion du cycle de vie (`start()`). De manière symétrique, vous pouvez relâcher les dépendances dans la méthode `stop()`.
- Les dépendances utiles à `SessionService` seront directement injectées dans le contexte par le serveur via des setters (vous comprendrez pourquoi plus loin : cela vous permettra de gérer la configuration de l'application au niveau du serveur).

Testez votre application. Vous pouvez ensuite par exemple vous servir du contexte pour filtrer les appels au DAO et ne renvoyer la bonne référence que si la méthode est appelée par une instance de type `SessionServiceRessource` (voir [ici](http://www.javalobby.org/java/forums/t67019.html) ou [là](http://stackoverflow.com/questions/421280/in-java-how-do-i-find-the-caller-of-a-method-using-stacktrace-or-reflection) pour des exemples de code sur comment trouver la classe appelant une méthode).

**Remarque** : dans ce cas, supprimez l'appel à la méthode `toString()` de l'instance du DAO dans l'affichage de la méthode `start()` des composants.

### 3.3 Généralisation du contexte

Actuellement, votre contexte n'est capable que de gérer un `SessionDAO` et un `ConnexionService`. Modifiez-en l'API pour qu'il puisse stocker des références à tous les types d'objets et que ces objets soient accessibles par un nom (String).

Testez ce contexte générique en lui injectant aussi les dépendances des autres objets de type  `XxxRessource` et  `XxxService` et en faisant dépendre ces objets du contexte.

> Votre serveur a désormais une responsabilité supplémentaire : en plus de fournir un conteneur de composants métier, il gère un contexte pour l'isolation des composants du conteneur. Le contexte est accessible à l'intérieur du conteneur pour permettre et contrôler l'accès aux composants. Vous avez mis en place les principaux éléments d'un framework applicatif, que vous allez perfectionner dans la suite.

## 4. Création d'un Annuaire

Actuellement, votre contexte est le même pour tous les objets, et "mélange" dans la même classe toutes ces dépendances. Vous allez y remédier dans cette partie.

### 4.1 Transformation du contexte en annuaire

Encapsulez le contexte dans une structure de type annuaire (cf. cours). Un annuaire sera une hiérarchie de contextes, spécifiques à différents éléments de votre conteneur et de vos applications. Ces éléments correspondront aux différents types d'objets à isoler (les `XxxRessource`, les `XxxService`, les DAOs). Faites en sorte que votre annuaire soit capable de décomposer les noms de la manière suivante : nom de contexte + "/" + nom d'objet stocké.

Vous allez maintenant commencer à remplir votre annuaire. Bindez :

- dans le contexte racine de l'annuaire : le conteneur
- dans un contexte spécifique à l'application : les objets "de premier niveau" (ici, les différents `XxxRessource`), c'est-à-dire qui répondent aux requêtes du client,
- dans un sous-contexte du précédent, spécifiquement dédié à la persistence : les instances des différents Dao
- dans un sous-contexte du contexte de l'application : les autres objets métier spécifiques à l'application

Remarque : les bindings dans l'annuaire s'effectuent avec des références à des instances déjà créées. Ces instances doivent donc être déjà créées (éventuellement par le conteneur), l'annuaire étant uniquement un moyen de permettre les accès à ces objets.

> Vous venez de construire quelque chose de similaire à un annuaire JNDI, qui pernet aux composants d'accéder à des références sur des objets interne au conteneur ou distants. L'avantage de cette méthode est qu'elle fonctionne quelles que soient les implémentations du conteneur et du composant, et qu'elle permet d'utiliser plusieurs implémentations différentes d'un objet pour une même interface.

### 4.2 Fusion avec le conteneur

Actuellement, vous avez un contexte qui gère les dépendances, en quelque sorte "à la place" du conteneur, prévu à cet effet. Il ne sert donc à rien d'utiliser deux outils distincts. Dans cette partie, vous allez conserver l'API de votre annuaire, mais l'implémenter en utilisant uniquement le conteneur.

**Attention** : dans cette partie, il faut conserver votre travail des étapes précédentes, mais remettre le référentiel de dépendances dans son état à la fin de la section 2 (avant que tout ne dépende que du contexte).

1. Choisissez un moyen de hiérarchiser vos composants dans le conteneur :
  - en utilisant des [scoped containers](http://picocontainer.com/scopes.html)
  - en nommant les paramètres quand vous utilisez la méthode [`addComponent()`](http://picocontainer.com/javadoc/core/org/picocontainer/MutablePicoContainer.html#addComponent(java.lang.Object,%20java.lang.Object,%20org.picocontainer.Parameter...)) (non testé mais plus simple)
2. Implémentez et exposez (sur le serveur) les méthodes `bind`, `rebind`, `unbind` et `lookup` de l'annuaire, pour qu'elles fassent le même travail que précédemment, mais en utilisant des composants du conteneur.
3. Dans la classe abstraite qui implémente `Startable`, rajoutez une variable `protected` de type `Annuaire`, et initialisez-la dans la méthode `start()`
4. Dans les implémentations de la méthode `start()` spécifiques des composants, rajoutez `super.start();`, de façons à ce qu'ils puissent tous utiliser l'annuaire.
5. Bindez les composants dans le conteneur comme cela était fait jusqu'à la section 2, c'est-à-dire avec dans les constructeurs, leurs dépendances entre eux, **à l'exception des paramètres de configuration** (les strings). &Agrave; la place, bindez ces paramètres dans le conteneur, et utilisez la méthode `start()` des composants pour leur permettre de les découvrir par eux-mêmes.
6. Faites en sorte que `StatisticsResource` découvre le nom de l'application dans l'annuaire, et l'indique dans son rapport.
7. Testez.

**Aide** : il y a possiblement (en tous cas c'était le cas les années précédentes) un bug dans la "MultiInjection" factory de PicoContainer. Donc l'injection dans les composants par setter / annotated field ne fonctionne pas en plus de celle par constructeur.
La solution (patch) que nous vous suggérons est d'utiliser un pattern singleton dans la méthode start() de la classe abstraite qui implémente Startable :
```java
protected Annuaire annuaire = null;
public void start() {
    this.annuaire = Annuaire.getInstance();
}
```
Et si on appelle cette classe `AbstractComponent`, elle ne contiendra que `start()` et `stop()`, et `AbstractController` dérivera de `AbstractComponent` et rajoutera la méthode `process(...)`.

> Vous avez maintenant "le meilleur des deux mondes" : un conteneur capable de résoudre les référentiels de dépendances, et de fournir un accès structuré (et protégé) aux composants applicatifs. Notez que pour que protéger cet accès, il faudrait _a minima_ vérifier les sources des appels à la méthode `lookup()`.

### 4.3 Aspects dynamiques de l'annuaire

Actuellement, vos objets interrogent l'annuaire pour récupérer des références à d'autres objets. Cependant, il peut arriver qu'une référence sur un objet change, par exemple parce qu'un objet n'est plus disponible ou qu'une nouvelle version a été implémentée. Vous allez donc mettre en place un système à base d'événements qui permettra aux objets clients de s'abonner aux changements des références dans l'annuaire et donc de faire une nouvelle requête à l'annuaire à chaque notification.

Pour cela, modifiez l'implémentation de votre annuaire :

- mettez en place un pattern Observer
- faites en sorte que les objets puissent s'abonner aux événements "changement de référence sur le nom X" pour réagir en conséquence
- déclenchez cet événement à chaque rebind d'un objet sur un nom existant

> Cette stratégie de mise à jour dynamique d'une référence sur un objet est celle utilisée dans les frameworks à composants dynamiques, comme OSGi. Bien entendu, cette stratégie fonctionnera d'autant mieux si le client s'attend à trouver une implémentation d'une interface et non une instance d'une classe.

## 5. Pooling 

Dans cette partie vous allez constituer un _pool_ de `Session` qui sera géré dans une nouvelle implémentation de `SessionDAO`.

- Créer une classe `SessionDAOPool` contenant une liste de sessions, et possédant un nombre de sessions maximum (récupéré dans l'annuaire)
- Y ajouter des méthodes pour récupérer une session (libre dans le pool, ou en l'instanciant dans la limite du nombre maximum), et relâcher une session (la remettre dans le pool).
- Renvoyer une erreur si le pool est "plein" et si aucune session n'est libre
- Binder dans l'annuaire cette nouvelle implémentation du DAO à la place de `SessionDAOImpl` ; si vous avez correctement réalisé la question 4.3, vous n'avez rien à faire : le composant `SessionService` (qui dépend de `SessionDAO`) rechargera automatiquement la nouvelle implémentation. Sinon, vous devrez probablement redémarrer votre serveur (si tant est que vous ayez réussi à faire le bind sans l'arrêter...).
- Tester le fonctionnement du pool dans un test unitaire.  

## 6. Mise en place d'un serveur d'applications

Dans cette partie, vous allez rendre votre serveur générique et lui permettre d'exécuter différentes applications.

### 6.1 Configuration de l'application

Écrivez un fichier de configuration en XML (ou JSON) et stockez-y les dépendances de valeurs (type d'objet DAO, nom du fichier de stockage) et les types d'objets `XxxRessource` correspondant à chaque commande (à la manière des fichiers web.xml utilisés dans un container de servlets). Ci-dessous un exemple de fichier de configuration :

```
{
  "application-config": {
    "name": "Gestion RDP",
    "business-components": [
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.resources.SessionRessource"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.resources.StatisticsRessource"}
    ],
    "service-components": [
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.services.SessionService"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.services.StatisticsService"}
    ],
    "persistence-components": [
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.dao.VMDAO"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.dao.SessionDAO"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.dao.ConnexionDAO"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.dao.FournisseurDAO"},
      {"class-name": "fr.univlyon1.tiw1.gestionrdp.dao.DBAccess",
        "params": [{
          "name": "url",
          "value": "jdbc:postgresql://localhost/gestionvm"
        },
        {
          "name": "user",
          "value": "gestionvm"
        },
        {
          "name": "password",
          "value": "gestionvmpwd"
        }]
      }
    ]
  }
}
```
Remarque : dans le fichier de configuration, vous pouvez également indiquer :

- les noms des composants pour leur stockage dans l'annuaire
- les règles de contrôle d'accès à faire appliquer aux contextes dans lesquels ils se trouvent

Utilisez ces données dans la classe Serveur lors de l'instanciation des éléments des conteneurs et du contexte. Vous devrez utiliser l'API Reflection (et probablement `Class.forName()`) pour récupérer le .class défini par une chaîne de caractères.

### 6.2 Spécialisation des composants (bonus)

Éventuellement, vous pouvez spécialiser vos classes `XxxRessource` et définir différents types de composants :

- Contrôleurs : ce seront les composants en front du serveur. Ils devront répondre à un type de requête du client, et organiser les traitements réalisés par les autres composants
- Vues : elles récupèreront les données fournies par les modèles et les formatteront pour qu'elles répondent aux attentes du client
- Modèles : ces composants réaliseront les traitements métier. Ils pourront faire appel à d'autres composants, de type entités
- Entités : ils représentent les données métier destinées à être liées à un support de persistance (`Abonne`, `EmpruntDTO`)

Pour mettre en oeuvre cette spécialisation, vous pouvez soit faire hériter chaque type d'une interface spécifique, soit les annoter en fonction d'annotations définies en conséquence.

De la même manière, pour prendre en compte cette spécialisation au niveau du serveur, vous pouvez soit modifier ce serveur (et son mode de configuration) pour que le fichier de configuration mentionne la nature des composants, et que le serveur la "comprenne", soit rajouter un composant intermédiaire qui intercepte toutes les requêtes et s'appuie sur son propre mode de configuration pour instancier et rediriger les requêtes sur ces composants.

> À ce stade, vous avez réalisé un serveur d'applications, composé d'un serveur et d'un framework capable de mettre en place et de faire tourner différents types d'applications. Si vous avez réalisé la partie 5.2 en modifiant le serveur, vous avez créé un serveur qui fonctionne d'une manière proche des serveurs Java EE. Si vous l'avez réalisée par ajout d'une couche supplémentaire entre le serveur de la question 4 et l'application, votre serveur se rapproche plus d'un conteneur Spring inclus dans un conteneur de servlets.

**To be continued...**

[rdp]: https://fr.wikipedia.org/wiki/Remote_Desktop_Protocol
[baremetal]: https://en.wikipedia.org/wiki/Bare-metal_server
