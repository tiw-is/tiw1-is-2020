Université Claude Bernard Lyon 1 – M2 TIW – Intergiciels et Services

**Ce TP est à rendre pour le dimanche 15 novembre 2020 à 23h59.**

# TP 3 : Mise en place d'un framework "moderne"

N'oubliez pas de faire une branche git `tp3` dans laquelle vous ferez toutes les modifications de ce TP.

## Objectifs pédagogiques

Utiliser des méthodes avancées pour la transformation d'une application générique en framework Web :

- Mettre en place la prise en charge de la stack web pour pouvoir répondre à des requêtes HTTP.
- Mettre en place des annotations pour générer du code "boilerplate" non spécifique à une application.

## Description du métier de l'application

Dans ce TP, vous repartirez du serveur d'applications développé au TP précédent. L'objectif est de poursuivre et de finaliser la construction d'un framework Web autour de cette application.

### Code fourni

Pour vous faire gagner du temps, nous vous fournissons deux projets Maven dans la branche `tp3` du [projet contenant les sources](https://forge.univ-lyon1.fr/tiw-is/tiw-is-2020-sources) :

- un projet nommé "base-processor" (à compiler et packager en premier) qui contient un processeur d'annotations très basique et que vous aurez à améliorer,
- un projet nommé "pico-jetty" (à lancer avec le goal `exec:java`) qui contient un jetty embarqué et une servlet à la racine du serveur ; cette servlet est annotée et vous servira de contrôleur principal.

En lançant le second projet, vous devez voir les logs affichés par le processeur d'annotations pendant la phase de compilation. Jetty doit se lancer et servir la servlet fournie à la racine du serveur.

**Attention : n'oubliez pas de supprimer le dossier `generated-sources` à chaque recompilation** de votre projet (`mvn clean compile` ou [`auto-clean`](https://maven.apache.org/plugins/maven-clean-plugin/usage.html)). Sans quoi au mieux, vous aurez un message d'erreur du compilateur, au pire rien du tout sauf des sources pas mises à jour et un projet qui sera figé dans l'état où il était la première fois que vous avez généré les sources avec le processeur d'annotations.

## Partie 1 : ajout de la stack Java EE Web

Comme vous l'avez compris, l'objectif est de transformer votre application de gestion de sessions en une application Web.

### 1.1 Initialisation

La servlet `MainController` aura donc la responsabilité de gérer la classe `Server` du TP2.

Faites en sorte que l'initialisation de l'application ait lieu au lancement de la servlet : comme au TP2, il faut que la configuration soit lue et que les composants soient instanciés par le conteneur en fonction de leurs dépendances.

### 1.2 Réponse aux requêtes HTTP

L'objectif est ici de répondre aux méthodes de service.

- Mettez en place un système de routage basé sur la forme des URLs, qui aiguillera les requêtes vers les bons composants applicatifs (i.e. les contrôleurs délégués)
- Désencapsulez les paramètres des requêtes et de les passer aux ressources de votre application Java dans le format choisi au TP2
- Faites en sorte que le format des paramètres dans les contenus des requêtes et des réponses soit du JSON
- Au niveau de la servlet, gérez les exceptions du package `fr.univlyon1.tiw1.gestionrdp.exception` en renvoyant des erreurs HTTP appropriées

### 1.3 Généralisation

Pour conserver le découplage de votre serveur et de l'application mis en place au TP2, faites en sorte que les méthodes HTTP, les routes et la gestion des exceptions soient inscrites dans la configuration et lues par le contrôleur principal à l'initialisation.

> Vous avez maintenant transformé votre serveur d'applications standalone en un framework Web.

## Partie 2 : Méta-programmation

Le projet `base-processor` comprend :

- une annotation `Component` très simple qui vous permettra de déclarer plusieurs types de composants dans votre application
- une énumération qui vous permet de spécifier un type de composant
- un processeur d'annotations qui s'applique à l'annotation @Component

**Dans cette partie, vous allez transformer ce projet qui contient originellement un processeur d'annotations en un projet permettant de mettre en place un framework Web. Pour que cela soit plus clair, renommez ce projet en `web-framework`.**

### 2.1 Processeur d'annotations "simple"

Modifiez le processeur d'annotations pour qu'il génère du code supplémentaire. L'objectif est ici de générer une sous-classe du composant avec les méthodes de gestion du cycle de vie `start()` et `stop()` qui font un log de `this.toString()` (voir cours).

Si votre classe abstraite `Component` rajoute d'autres éléments (champs, méthodes), générez-les également.

**Aide** : il est conseillé d'utiliser :

- [JavaPoet](https://github.com/square/javapoet) pour la génération des fichiers Java (mais vous pouvez aussi générer toutes les sources "à la main" comme dans [ce tuto](https://www.baeldung.com/java-annotation-processing-builder)).
- l'interface [Filer](http://cr.openjdk.java.net/~iris/se/11/build/latest/api/java.compiler/javax/annotation/processing/Filer.html) pour l'écriture des fichiers dans le répertoire `generated-sources`

Compilez votre projet `pico-jetty`, et regardez dans `target/generated-sources` si le fichier y est. Vérifiez qu'il est conforme à vos attentes.

### 2.2 Héritage d'annotations

De la même manière, créez des annotations `@controller`, `@Service`, `@Persistence`, et les processeurs correspondants. Générez le code spécifique à ces types de composants, en fonction du fonctionnement de votre serveur.

**Remarques** :

- Ce code ne doit pas être redondant avec celui de `@Component`. Puisque plusieurs "rounds" de traitement peuvent être faits par les différents processeurs, générez des classes annotées par `@Component` et laissez faire le premier processeur.
- Pour l'instant, les processeurs des annotations `@controller`, `@Service`, `@Persistence` génèrent des sous-classes des composants sans y ajouter de code (uniquement une annotation). Ne vous en préoccupez pas, ce comportement sera modifié à la question suivante.

> Normalement, vous avez maintenant des sous-classes de vos composants comportant dans leur code toutes les méthodes que vous aviez précédemment ajoutées dans les classes abstraites. Ces classes abstraites ne sont donc plus nécessaires.

Dans votre serveur, faites en sorte d'utiliser les classes générées, plutôt que celles qui implémentent les classes abstraites. Supprimez ensuite le lien avec les classes abstraites pour revenir à des composants "POJO".

### 2.3 Annotations de configuration

Rajoutez à vos annotations des méthodes qui permettent de définir les paramètres de configuration dont vous avez besoin pour faire fonctionner vos objets dans votre serveur (nom des méthodes des contrôleurs...). Générez des champs correspondant à ces paramètres, et les getters permettant d'y accéder. Si ces paramètres sont consommés par le serveur, mettez ces getters en public.

> &Agrave; ce stade, vous ne devriez plus avoir besoin du fichier de configuration, toutes les informations qu'il contient ayant été déplacées dans des annotations. Votre application doit être beaucoup plus facilement compréhensible et comporter des composants aux types clairement identifiés. Cependant, vous avez toujours dans le même projet l'application et le framework...

### 2.4 Génération d'un framework applicatif

Créez une classe `Main` dans votre projet `pico-jetty`, qui lancera le goal `exec:java`. Votre serveur doit démarrer et servir votre application au lancement de cette classe.

Créez maintenant dans le projet `web-framework` une nouvelle annotation et le processeur qui génère le serveur et le reste des classes du framework nécessaires pour démarrer l'application. Dans le projet `pico-jetty`, débarrassez-vous de toutes les classes non métier (à part `Main`), devenues inutiles. Annotez votre classe `Main` avec cette nouvelle application.

Et voilà...